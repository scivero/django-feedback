publish:
	if [ $(git branch --show-current) = "master" ]; then
		poetry version
		git tag $(grep -Po '(?<=version = "")(\d+\.\d+\.\d+)(?=")' pyproject.toml)
		git push --tags
	else
		echo "You can only publish from the master branch"
	fi
